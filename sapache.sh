#!/bin/bash

start_apache() {

    pdoc --output /Users/maximedevillepoix/Cours_EPSI/Eval_CI/workspace /Users/maximedevillepoix/Cours_EPSI/Python/Exercices/tirage_groupes/fonctions/app_fonction.py
    
    docker run -d -p 8082:80 -v /Users/maximedevillepoix/Cours_EPSI/Eval_CI/workspace:/usr/local/apache2/htdocs --name my-apache httpd:latest
    echo 'Apache démarré'
}

stop_apache() {
    # Arrêter le conteneur Apache
    docker stop my-apache
    docker rm my-apache
    echo 'Apache arrêté'
}

case "$1" in
    start)
        start_apache
        ;;
    stop)
        stop_apache
        ;;
    *)
        echo 'Usage: $0 [start|stop]'
        exit 1
        ;;
esac
