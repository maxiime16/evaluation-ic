#!/bin/bash

if [ $# -ne 2 ]; then
    echo "Usage: $0 [nom_du_fichier_zip] [répertoire_à_zipper]"
    exit 1
fi

# Utilisation de 7zip pour compresser le répertoire spécifié
7z a "$1.zip" "$2"

echo "Artefact de livraison créé : $1.zip à partir du répertoire $2"
