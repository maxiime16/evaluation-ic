#!/bin/bash

start_jenkins() {
    JENKINS_HOME="/Users/maximedevillepoix/Cours_EPSI/Eval_CI/workspace"
    java -jar ${JENKINS_HOME}/jenkins.war --httpPort=9090 --prefix=/jenkins
    echo 'Jenkins démarré sur le port 9090 avec le contexte /jenkins'
}

stop_jenkins() {
    echo 'Jenkins arrêté'
}

case "$1" in
    start)
        start_jenkins
        ;;
    stop)
        stop_jenkins
        ;;
    *)
        echo "Usage: $0 [start|stop]"
        exit 1
        ;;
esac
